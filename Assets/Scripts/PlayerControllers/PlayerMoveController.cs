﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveController : MonoBehaviour
{
    [HideInInspector] public bool disable = true;

    public float speed;

    public int leftMaxRotation;

    public int rigthMaxRotation;

    public int backRotation;

    Rigidbody playerRigidbody; 

    int floorMask;

    float camRayLength = 100f;  

    private int back;

    void Awake()
    {
        floorMask = LayerMask.GetMask ("Floor");
        playerRigidbody = GetComponent<Rigidbody>();
        back = -Mathf.RoundToInt(transform.forward.z);
    }

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        if(!disable){
            Move(h, v);

            Turning();
        }   
    }

    void Move(float h, float v)
    {
        Vector3 movement = new Vector3();
        movement.Set(h, 0f, v);

        movement = movement.normalized * speed * Time.deltaTime;
        
        if(v == back){
           playerRigidbody.constraints = RigidbodyConstraints.FreezeRotation; 
        }

        playerRigidbody.MovePosition(transform.position + movement);

    }

    void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            
            playerToMouse.y = 0f;

            Quaternion newRotatation = Quaternion.LookRotation(playerToMouse);
            if(newRotatation.eulerAngles.y > rigthMaxRotation && newRotatation.eulerAngles.y < backRotation){
                newRotatation.eulerAngles = new Vector3(0f,rigthMaxRotation,0f);
            }else if(newRotatation.eulerAngles.y < leftMaxRotation && newRotatation.eulerAngles.y > backRotation){
                newRotatation.eulerAngles = new Vector3(0f,leftMaxRotation,0f);
            }            
            playerRigidbody.MoveRotation(newRotatation);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("floor")){
            disable= false;
        }
    }

}