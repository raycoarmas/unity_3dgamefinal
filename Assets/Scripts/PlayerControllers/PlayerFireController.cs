﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFireController : MonoBehaviour {

    private AudioSource[] audios;
    
	public GameObject gun;

    public bool singlePlayer;

    [HideInInspector] public AudioSource audioWall;
    void Start()
    {
        audios = GetComponents<AudioSource>();
    }
	void FixedUpdate()
    {
        fire();
    }

	 void fire()
    {
        int audio = Random.Range(0, audios.Length);

        bool wallSound = (audioWall != null ? audioWall.isPlaying : false);

        if (Input.GetAxisRaw("Fire1") > 0 && gun.transform.childCount > 0 && !singlePlayer)
        {	
            audios[audio].Play();
            Transform ball = gun.transform.GetChild(0);
			ball.SetParent(null);
			Rigidbody ballRgb = ball.GetComponent<Rigidbody>();
			ballRgb.isKinematic = false;
            ballRgb.AddForce(transform.forward * 500f);
			
        }
        if(singlePlayer && gun.transform.childCount > 0 && !wallSound){
            audios[audio].Play();
            Transform ball = gun.transform.GetChild(0);
			ball.SetParent(null);
			Rigidbody ballRgb = ball.GetComponent<Rigidbody>();
			ballRgb.isKinematic = false;
            ballRgb.AddForce(transform.forward * 500f);
        }
    }
}
