﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitController : MonoBehaviour {

	[HideInInspector] public GameObject audios;
	
	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.CompareTag("ball")){
			AudioSource[] audioSources =audios.GetComponents<AudioSource>();
			audioSources[Random.Range(0, audioSources.Length)].Play();
			Destroy(this.gameObject);
			Destroy(other.gameObject);
		}
	}
}
