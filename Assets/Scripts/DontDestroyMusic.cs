﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyMusic : MonoBehaviour
{

    private static DontDestroyMusic instance = null;
    public static DontDestroyMusic Instance
    {
        get { return instance; }
    }
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
            return;
        }
        else
        {
            instance = this;
			AudioSource source = GetComponent<AudioSource>();
       		source.Play();
        }
        DontDestroyOnLoad(this);
    }
}
