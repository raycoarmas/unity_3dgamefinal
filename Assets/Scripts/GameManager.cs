﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public GameObject deathSounds;

	public GameObject player;
	
	public Transform spawnPointBlue;

	public Transform spawnPointRed;

	public Color blue;

	public Color red;

	public GameObject ball;

	private PlayerMoveController playerMoveRed;
	
	private GameObject[] playersSpawned = new GameObject[2];
	public ParticleSystem[] particleSystems;

	public WallController wallRed;
	
	public WallController wallBlue;	
	
	private bool gameReady = false;
	
	private bool singlePlayer = true;

	public Transform leftWall;

	public Transform rightWall;

	public Transform backBlueWall;

	public Transform middleWall;
	
	private EnemyMoveController enemyMove = null;

	public GameObject endPanel;

	public Text messages;

	public GameObject particles;

	void Start () {
		GameObject bluePlayer = Instantiate(player, spawnPointBlue.position, spawnPointBlue.rotation);

        bluePlayer.GetComponentInChildren<MeshRenderer>().material.color = blue;
		wallBlue.player = bluePlayer;
		bluePlayer.GetComponent<PlayerHitController>().audios = deathSounds;

		GameObject redPlayer = Instantiate(player, spawnPointRed.position, spawnPointRed.rotation);
		
		redPlayer.GetComponentInChildren<MeshRenderer>().material.color = red;
		wallRed.player = redPlayer;
		redPlayer.GetComponent<PlayerHitController>().audios = deathSounds;

		playersSpawned[0] = bluePlayer;
		playersSpawned[1] = redPlayer;

		if (singlePlayer){
			enemyMove = bluePlayer.AddComponent<EnemyMoveController>();
			enemyMove.speed = 6;
			enemyMove.newTarget = 1;
			enemyMove.leftWall = rightWall.position;
			enemyMove.rightWall = leftWall.position;
			enemyMove.backWall = backBlueWall.position;
			enemyMove.middleWall = middleWall.position;
			enemyMove.objetive = redPlayer.transform;
			PlayerFireController firecontroller = bluePlayer.GetComponent<PlayerFireController>();
			firecontroller.singlePlayer = singlePlayer;
		}else{
			bluePlayer.AddComponent<PlayerMoveController>();
			PlayerMoveController playerMoveBlue = bluePlayer.GetComponent<PlayerMoveController>();
			playerMoveBlue.speed = 6;
			playerMoveBlue.rigthMaxRotation = 225;
			playerMoveBlue.leftMaxRotation = 135;
			playerMoveBlue.backRotation = 0;
		}
		redPlayer.AddComponent<PlayerMoveController>();
		playerMoveRed = redPlayer.GetComponent<PlayerMoveController>();
		playerMoveRed.speed = 6;
		playerMoveRed.leftMaxRotation = 315;
		playerMoveRed.rigthMaxRotation = 45;
		playerMoveRed.backRotation = 180;

		redPlayer.GetComponent<NavMeshAgent>().enabled=false;
	}
	
	void Update () {
		if(!playerMoveRed.disable && !gameReady){
			Transform gun = playersSpawned[1].transform.Find("Gun");
			ball = Instantiate(ball, gun.position, gun.rotation);
			ball.transform.SetParent(gun);
			gameReady = true;

		}

		if(playersSpawned[0] == null){
            messages.text = "Red won";
            endPanel.SetActive(true);
			particles.SetActive(true);
			foreach (ParticleSystem item in particleSystems)
			{
                ParticleSystem.MainModule mainModule = item.main;
				mainModule.startColor = new ParticleSystem.MinMaxGradient(red);	
			}
			playersSpawned[1].GetComponent<PlayerMoveController>().speed = 0;
			
		}
		if(playersSpawned[1] == null){
            messages.text = "Blue won";
            endPanel.SetActive(true);
			particles.SetActive(true);
			foreach (ParticleSystem item in particleSystems)
			{
                ParticleSystem.MainModule mainModule = item.main;
				mainModule.startColor = new ParticleSystem.MinMaxGradient(blue);
			}
			if(singlePlayer){
				playersSpawned[0].GetComponent<EnemyMoveController>().speed = 0;
			}else{
				playersSpawned[0].GetComponent<PlayerMoveController>().speed = 0;
			}
		}
	}

	public void RestGame(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void MainMenu(){
        SceneManager.LoadScene("MainMenu");
    }
}
