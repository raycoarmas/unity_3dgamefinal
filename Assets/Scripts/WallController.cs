﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour {

	[HideInInspector] public GameObject player;
	
	public GameObject ball;
	
	private AudioSource audioWall;
	void Start () {
		audioWall =	GetComponent<AudioSource>();
	}
	
	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.CompareTag("ball")){
			audioWall.Play();
			Destroy(other.gameObject);
			Transform gun = player.transform.Find("Gun");
			player.GetComponent<PlayerFireController>().audioWall = audioWall;
			GameObject ballSpawned = Instantiate(ball, gun.position, gun.rotation);
			ballSpawned.transform.SetParent(gun);
		}
	}

	void OnTriggerEnter(Collider other)
	{	
		if(other.gameObject.CompareTag("Player")){
			Rigidbody otherRB= other.gameObject.GetComponent<Rigidbody>();
			otherRB.constraints = otherRB.constraints | RigidbodyConstraints.FreezePositionZ;
		}
	}
}
