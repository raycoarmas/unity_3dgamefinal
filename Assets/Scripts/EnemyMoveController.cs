﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMoveController : MonoBehaviour
{

    private float timer;

    public int newTarget;

    public float speed;

    private NavMeshAgent navMeshAgent;

    private Vector3 target;

    [HideInInspector] public Vector3 leftWall;
    [HideInInspector] public Vector3 rightWall;
    [HideInInspector] public Vector3 middleWall;
    [HideInInspector] public Vector3 backWall;
	
    [HideInInspector] public Transform objetive;


    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.updateRotation = false;
    }

    void Update()
    {
        navMeshAgent.speed = speed;
        timer += Time.deltaTime;
        if (timer >= newTarget)
        {
            generateNewTarget();
            timer = 0;
        }
        Turning();
    }

    void generateNewTarget()
    {

        float xPos = Random.Range(leftWall.x, rightWall.x);
        float zPos = Random.Range(middleWall.z, backWall.z);

        target = new Vector3(xPos, transform.position.y, zPos);

        navMeshAgent.SetDestination(target);

    }

    void Turning()
    {   
        if(objetive != null){
	    	transform.LookAt(objetive.position);
        }
    }
}
